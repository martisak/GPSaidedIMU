# Sensor fusion with GPS and IMU
Assignment for the WASP course WASP Autonomous Systems I

## Introduction

In this assignment we study an inertial navigation system (INS) using sensor fusion by Kalman filtering.

- We evaluate the effects of a GPS signal outage.
- Implement an improved vehicle motion model that doesn't allow a car to skid or fly.
- Include measurement from a speedometer.

Description and instructions can be found in [Sensor fusion with GPS and IMU](https://kth.instructure.com/courses/4962/files/805888/download?verifier=0pYkYUwoZPMGlukMWCScvyVnXQY5br6Bxmqhpvvk&wrap=1).
 
## GPS signal from experiment

- The car drives for about 5 minutes. Note the round-about in the middle of the path. The data resolution is (in general) 1 Hz for GPS, 100 Hz for IMU and 4 Hz for speedometer.

<img src="figures/fig_2_orig.png" width="33%" align="left" />
<img src="figures/fig_3_orig.png" width="33%" align="left" />
<img src="figures/fig_4_orig.png" width="33%" align="left" />

- The car starts in the origin, travels northeast and turns back towards the start.

## Acknowledgments

This work was partially supported by the Wallenberg AI, Autonomous Systems and Software Program (WASP) funded by the Knut and Alice Wallenberg Foundation.
