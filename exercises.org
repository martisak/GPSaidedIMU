* Exercises

- [ ] Task 1 - Error growth.
  Use the functions `errorgrowth.m and Nav eq.m to evaluate how the position error grows with time. Assume that the navigation system is sta- tionary; the initial position and velocity is zero; the initial roll, pitch, and yaw are zero; the accelerometer measurements are error free; and the gyroscope measurements are error free except from a bias in the x-axis gyroscope with a magnitude of 0.01 degree/s. Explain the observed behavior: In what directions do you get a position error? Find an approximative formula for the error behavior. Where is error largest, Stockholm or Lund?

- [ ] Task 2
  Familiarize yourself with the Matlab code that implements the GNSS-aided INS and execute the script main.m. Modify the code to simulate a GNSS-receiver outage from 200 seconds and onward. Plot the estimated car position xˆk (outputdata.xh). Also plot the difference between estimated car position and the GPS position without outage. Experiment with varying settings of filter parameters in get settings.m and try to improve the filter performance during GPS outage.

- [ ] Task 3
  Implement support for non-holonomic motion constraints and speedometer measurements into the GNSS-aided INS framework. Some hints where changes are needed are given in the code. Turn off speedometer measurements and run with only the motion constraints (settings.speed aiding=’off’;settings.non holonomic=’on’ ). Tune the filter by changing the magnitude of filter parameters, for example R(3), until you get a decent performance and plot the position estimates and po- sition error, with GNSS outage. You should see a substantial improvement

- [ ] Task 4
  The data included in the GNSSaidedINS.zip folder also include measurements from a speedometer. Let the fusion filter also use these mea- surements, trim the filter, and then plot the position estimates and position error, with GNSS outage. What is the resulting rms error of the position tra jectory?

* Project report

Describe your results in a report and submit in Canvas before the deadline. You do not need to submit any Matlab code, unless you for some reason want to.

