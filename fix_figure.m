% fix_figures

% Font size
set(gca,'fontsize',20)

% All lines
lines = findobj(gcf,'Type','Line');
for i = 1:numel(lines)
  lines(i).LineWidth = 2.0;
end

% Axes
set(gca,'Linewidth',1)
grid off
box off