%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                           
% Main script for the loosely-coupled feedback GNSS-aided INS system. 
%  
% Edit: Isaac Skog (skog@kth.se), 2016-09-01,  
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
close all

%% Load data
disp('Loads data')
load('GNSSaidedINS_data.mat');

%% Original
disp('Original')

settings=get_settings();
settings.non_holonomic = 'off';
settings.speed_aiding = 'off';
settings.gnss_outage = 'off'; 

out_data=GPSaidedINS(in_data,settings);
plot_data(in_data,out_data,'True');drawnow
% fix figure
for f=2:8
    figure(f)
    fix_figure
    saveas(gcf,['fig_', num2str(f), '_orig.png'])
end

%% Task 2 (GNSS outage)
disp('Task 2 (GNSS outage)')
close all
settings=get_settings();
settings.non_holonomic = 'off';
settings.speed_aiding = 'off';
settings.gnss_outage = 'on'; 

out_data=GPSaidedINS(in_data,settings);
plot_data(in_data,out_data,'True');drawnow

% fix figure
for f=2:8
    figure(f)
    fix_figure
    saveas(gcf,['fig_', num2str(f), '_task2.png'])
end

%% Task 2 (GNSS outage)
disp('Task 2 (GNSS outage)')

close all
settings=get_settings();
settings.non_holonomic = 'off';
settings.speed_aiding = 'off';
settings.gnss_outage = 'on';

fun = @(x)objective(x,in_data, settings);
options = optimset('PlotFcns',@optimplotfval);
v = [settings.sigma_non_holonomic settings.sigma_speed settings.sigma_gps];
a = fminsearch(fun,v, options);

fix_figure
saveas(gcf,'opt_task2_opt.png')

settings.sigma_non_holonomic = a(1);
settings.sigma_speed = a(2);
settings.sigma_gps = a(3);

out_data=GPSaidedINS(in_data,settings);
plot_data(in_data,out_data,'True');drawnow

%%

% fix figure
for f=2:8
    figure(f)
    fix_figure
    saveas(gcf,['fig_', num2str(f), '_task2_opt.png'])
end

%% Task 3 (Non-holonomic)
disp('Task 3 (Non-holonomic)')
close all
settings=get_settings();
settings.non_holonomic = 'on';
settings.speed_aiding = 'off';
settings.gnss_outage = 'on'; 

fun = @(x)objective(x,in_data, settings);
options = optimset('PlotFcns',@optimplotfval);
v = [settings.sigma_non_holonomic settings.sigma_speed settings.sigma_gps];
a = fminsearch(fun,v, options);

fix_figure
saveas(gcf,'opt_task3_opt.png')

settings.sigma_non_holonomic = a(1);
settings.sigma_speed = a(2);
settings.sigma_gps = a(3);

out_data=GPSaidedINS(in_data,settings);
plot_data(in_data,out_data,'True');drawnow

% fix figure
for f=2:8
    figure(f)
    fix_figure
    saveas(gcf,['fig_', num2str(f), '_task3.png'])
end


%% Task 4 (Non-holonomic, speedometer)
disp('Task 4 (Non-holonomic, speedometer)')
close all
settings=get_settings();
settings.non_holonomic = 'on';
settings.speed_aiding = 'on';
settings.gnss_outage = 'on'; 

fun = @(x)objective(x,in_data, settings);
options = optimset('PlotFcns',@optimplotfval);
v = [settings.sigma_non_holonomic settings.sigma_speed settings.sigma_gps];
a = fminsearch(fun,v, options);

fix_figure
saveas(gcf,'opt_task4_opt.png')

settings.sigma_non_holonomic = a(1);
settings.sigma_speed = a(2);
settings.sigma_gps = a(3);

out_data=GPSaidedINS(in_data,settings);
plot_data(in_data,out_data,'True');drawnow

% fix figure
for f=2:8
    figure(f)
    fix_figure
    saveas(gcf,['fig_', num2str(f), '_task4.png'])
end

