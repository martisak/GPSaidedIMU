function b = objective(v, in_data, settings)

    settings.sigma_non_holonomic = v(1);
    settings.sigma_speed = v(2);
    settings.sigma_gps = v(3);

    out_data=GPSaidedINS(in_data,settings);
    b = get_error(in_data,out_data);