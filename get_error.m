function positionerr_RMS = get_error(in_data,out_data)
xest = out_data.x_h(2,:);
yest = out_data.x_h(1,:);
xgps = interp1(in_data.GNSS.t,in_data.GNSS.pos_ned(2,:),in_data.IMU.t,'linear','extrap')';
ygps = interp1(in_data.GNSS.t,in_data.GNSS.pos_ned(1,:),in_data.IMU.t,'linear','extrap')';
xerr = xest - xgps; 
yerr = yest - ygps;
positionerr_RMS = sqrt(mean(xerr.^2+yerr.^2));
